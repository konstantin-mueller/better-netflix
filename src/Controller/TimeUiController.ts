import TimeModel from "../Model/TimeModel"
import { options } from "../Constants/Options"

class TimeUiController {
    private readonly _timeModel: TimeModel

    private _htmlTime: HTMLElement
    private _video: HTMLVideoElement

    constructor() {
        this._timeModel = new TimeModel()
    }

    public initTime(video: HTMLVideoElement): void {
        if (
            !options.timeElapsed ||
            document.querySelector("time.elapsedTime") ||
            !video
        ) {
            return
        }

        // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
        const parent = document.querySelector("div.ltr-lb3bic") as HTMLElement
        if (parent) {
            this._video = video
            this.initElapsedTime(parent)
        }
    }

    private initElapsedTime(parent: HTMLElement): void {
        this._htmlTime = document.createElement("time")
        this._htmlTime.classList.add("elapsedTime")
        this.updateTime()

        parent.style.display = "block"
        parent.prepend(document.createElement("hr"))
        parent.prepend(this._htmlTime)

        this._video.addEventListener(
            "timeupdate",
            () => {
                this.updateTime()
            },
            false,
        )
    }

    private updateTime(): void {
        if (this._video !== undefined) {
            this._timeModel.setCurrentTime(this._video.currentTime)
            this._htmlTime.textContent = this._timeModel.toString()
        }
    }
}

export default TimeUiController
