import VideoController from "./VideoController"
import ContainerElement from "../Ui/ContainerElement"
import UiButtonController from "./UiButtonController"

class UiController {
    public createUi(videoController: VideoController): void {
        const netflixButtons = this.getNetflixButtonParent()
        if (!netflixButtons || document.querySelector(".uiContainer")) return

        const uiContainer: ContainerElement =
            new UiButtonController().initButtons(videoController)

        netflixButtons.insertBefore(
            uiContainer.element,
            netflixButtons.children[0],
        )
    }

    public updateTitle(): void {
        const videoTitle = this.getVideoTitle()
        if (!videoTitle) return

        const titleChildren = videoTitle.childNodes
        if (!titleChildren || titleChildren.length === 0) return

        const title = Array.from(titleChildren)
            .map((child) => child.textContent)
            .join(" ")

        document.title = title
    }

    private getNetflixButtonParent(): HTMLDivElement {
        return document.querySelector(
            "div.watch-video--bottom-controls-container > div > div > div > div > div:nth-child(3)",
        )
    }

    private getVideoTitle(): HTMLDivElement {
        return document.querySelector(
            "div.watch-video--bottom-controls-container > div > div > div:nth-child(3) > div > div:nth-child(2) > div:nth-child(2) > div",
        )
    }
}

export default UiController
