import { options } from "../../Constants/Options"
import VideoController from "../VideoController"
import IAction from "./IAction"

export default class ToggleSubtitleAction implements IAction {
    key: string = options.toggleSubtitles

    private readonly subtitleButtonSelector =
        "div.watch-video--bottom-controls-container > div > div > div:nth-child(3) > div > div:nth-child(3) > div:nth-child(8) > button > div"

    private readonly subtitlesSelector =
        "div.show > div > div > div:nth-child(2) > ul > li"

    private readonly subtitleTextSelector = "div > div"

    private readonly textSubtitlesOff = ["off", "aus"]

    public execute(videoController: VideoController): void {
        let subtitleButton: HTMLDivElement = document.querySelector(
            this.subtitleButtonSelector,
        )
        if (!subtitleButton) {
            videoController.getHtmlVideo.click()
            this.waitForSubtitleMenu()

            subtitleButton = document.querySelector(this.subtitleButtonSelector)
        }

        subtitleButton.click()

        let retries = 10
        const selectSubtitleInterval = setInterval(() => {
            const successfullySelectedSubtitle = this.selectSubtitle()
            if (!successfullySelectedSubtitle && retries > 0) {
                retries -= 1
                return
            }

            clearInterval(selectSubtitleInterval)

            videoController.getHtmlVideo.click()
            videoController.getHtmlVideo.focus()
        }, 300)
    }

    private waitForSubtitleMenu() {
        let retries = 20
        const interval = setInterval(() => {
            const subtitleButton: HTMLDivElement = document.querySelector(
                this.subtitleButtonSelector,
            )
            if (!subtitleButton && retries > 0) {
                retries -= 1
                return
            }

            clearInterval(interval)
        }, 300)
    }

    private selectSubtitle() {
        const subtitles: NodeListOf<HTMLDivElement> = document.querySelectorAll(
            this.subtitlesSelector,
        )
        if (subtitles.length <= 1) {
            return false
        }

        const subtitleTexts: HTMLElement[] = Array.from(
            document.querySelectorAll(
                `${this.subtitlesSelector} > ${this.subtitleTextSelector}`,
            ),
        )

        if (subtitles[0].dataset.uia.includes("selected")) {
            let index = subtitleTexts.findIndex((element) =>
                element.textContent.toLowerCase().startsWith("englis"),
            )
            if (index === 0) {
                index = subtitleTexts.findIndex((element) =>
                    this.textSubtitlesOff.includes(
                        element.textContent.toLowerCase(),
                    ),
                )
            }
            index = index < 1 ? 1 : index

            subtitleTexts[index].click()
        } else {
            subtitleTexts[0].click()
        }

        return true
    }
}
