import { UserOptions } from "../Constants/Options"

class ChromeController {
    public getSync(obj: UserOptions, func: (items: UserOptions) => void): void {
        chrome.storage.sync.get(obj as Partial<UserOptions>, func)
    }
}

export default ChromeController
