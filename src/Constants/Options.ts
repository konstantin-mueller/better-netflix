import UserOptionsModel from "../Model/UserOptionsModel"

interface UserOptions {
    // Keys:
    zoomIn: string
    zoomOut: string
    resetZoom: string
    fullZoom: string
    disableMouse: string
    enableMouse: string
    toggleStatistics: string
    customZoom: string
    toggleSubtitles: string

    // Enabled / disabled options:
    volumeMouseWheel: boolean
    hideZoomInButton: boolean
    hideZoomOutButton: boolean
    hideResetZoomButton: boolean
    hideFullZoomButton: boolean
    showCustomZoomButton: boolean
    customZoomAmount: number
    hidePictureInPictureButton: boolean
    autoSkip: boolean
    autoSkipRecap: boolean
    elapsedTime: boolean
    updateTabTitle: boolean
}

const defaultKeys: UserOptions = {
    zoomIn: "+",
    zoomOut: "-",
    resetZoom: ",",
    fullZoom: ".",
    disableMouse: "d",
    enableMouse: "e",
    toggleStatistics: "q",
    customZoom: "c",
    toggleSubtitles: "v",
    volumeMouseWheel: true,
    hideZoomInButton: false,
    hideZoomOutButton: false,
    hideResetZoomButton: false,
    hideFullZoomButton: false,
    showCustomZoomButton: false,
    customZoomAmount: 0,
    hidePictureInPictureButton: false,
    autoSkip: false,
    autoSkipRecap: false,
    elapsedTime: true,
    updateTabTitle: true,
}

const options: UserOptionsModel = UserOptionsModel.optionKeys

export { defaultKeys, options, UserOptions }
