const config = {
    semi: false,
    tabWidth: 4,
}

export default config
